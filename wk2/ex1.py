#!/usr/bin/env python
#I. Create a script that does the following
#   A. Prompts the user to input an IP network.
#       Notes: 
#       1. For simplicity the network is always assumed to be a /24 network
#       2. The network can be entered in using one of the following three formats 10.88.17.0, 10.88.17., or 10.88.17

#   B. Regardless of which of the three formats is used, store this IP network as a list in the following format ['10', '88', '17', '0'] i.e. a list with four octets (all strings), the last octet is always zero (a string).

#       Hint: There is a way you can accomplish this using a list slice.

#       Hint2: If you can't solve this question with a list slice, then try using the below if statement (note, we haven't discussed if/else conditionals yet; we will talk about them in the next class).

# >>>> CODE <<<<

# Take the raw binary value 

rawoctets = raw_input('Octets please: ')

# Convert the binary string into a list

listoctets = rawoctets.split('.')

# Append a 0 or replace the last octet with a 0

if len(listoctets) == 3:
    listoctets.append('0')

elif len(listoctets) == 4:
    listoctets[3] = '0'

# >>>> END <<<<

#    C. Print the IP network out to the screen.

listoctets = '.'.join(listoctets)

print listoctets


#   D. Print a table that looks like the following (columns 20 characters in width):

tn = 'NETWORK_NUMBER'
tb = 'FIRST_OCTET_BINARY'
th = 'FIRST_OCTET_HEX'
ipaddress =  '88.19.107.0'
nb = '0b1011000'
nh = '0x58'

print '%20s %20s %20s' % (tn, tb, th)
print '%20s %20s %20s' % (ipaddress, nb, nh)
