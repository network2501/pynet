#!/usr/bin/env python
# Prompt a user for an IP address in dotted decimal format.
#  B. Convert this IP address to binary and display the binary result on the screen (a binary string for each octet).
#  Example output:
#   first_octet    second_octet     third_octet    fourth_octet
#       0b1010       0b1011000           0b1010         0b10011
# Take the raw binary value 

ipaddr = raw_input('IP address please: ')

# Convert the binary string into a list

list_ipaddr = ipaddr.split('.')

# Append a 0 or replace the last octet with a 0
a = bin(int(list_ipaddr[0]))
b = bin(int(list_ipaddr[1]))
c = bin(int(list_ipaddr[2]))
d = bin(int(list_ipaddr[3]))

#print ipaddr

# >>>> END <<<<
print '%20s %20s %20s %20s' % ("first_octet", "second_octet", "third_octet", "fourth_octet")
print '%20s %20s %20s %20s' % (a,b,c,d)
