#!/usr/bin/env python

a = "*  1.0.192.0/18   157.130.10.233        0 701 38040 9737 i"
b = "*  1.1.1.0/24      157.130.10.233         0 701 1299 15169 i"
c = "*  1.1.42.0/24     157.130.10.233        0 701 9505 17408 2.1465 i"
d = "*  1.0.192.0/19   157.130.10.233        0 701 6762 6762 6762 6762 38040 9737 i"

split_a = a.split()
split_b = b.split()
split_c = c.split()
split_d = d.split()

print '%-15s %-15s' % ('ip_prefix', 'as_path')
print '%-15s %-15s' % (split_a[1], split_a[4:-1])
print '%-15s %-15s' % (split_b[1], split_b[4:-1])
print '%-15s %-15s' % (split_c[1], split_c[4:-1])
print '%-15s %-15s' % (split_d[1], split_d[4:-1])
