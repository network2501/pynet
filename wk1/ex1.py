#!/usr/bin/env python

# Use the split method to divide the following IPv6 address into groups of 4 hex digits (i.e. split on the ":")
# FE80:0000:0000:0000:0101:A3EF:EE1E:1719
a = 'FE80:0000:0000:0000:0101:A3EF:EE1E:1719'
a.split(':')
b = a.split(':')
print 'IPv6 address split into a list using a.split(\':\')'
print b

# Use the join method to reunite your split IPv6 address back to its original value.
b = a.split(':')
print 'The previous list joined back together again with \':\'.join(b) where b = a.split(\':\')'
print ':'.join(b)
